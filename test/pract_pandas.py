from nltk.tokenize import word_tokenize
import pandas as pd
from pandas.core.groupby import grouper
from pandas.io.formats import style

# Series
series_test = pd.Series([100,200,300])
# print(series_test)

series_test2 = pd.Series(
                            {
                                1999:48,
                                2000:65,
                                2001:89
                            }
                        )
# print(series_test2)

# Dataframe
frame_test = pd.DataFrame(
                            {
                                1999:[78,89,45],
                                2000:[78,89,45],
                                20001:[78,89,45]
                            }
                        )
# print(frame_test)

frame_test2 = pd.DataFrame(
                            [
                                [78,89,45],
                                [78,89,45],
                                [78,89,45]
                            ],
                            columns=[1999,2000,2001],
                            index=['a', 'b', 'c']
                        )
# print(frame_test2)

# Read data

# pd.options.display.max_rows = 10 # ver únicamente 10 datos
el_universal= pd.read_csv('el_universal_2021_05_19_articles.csv', encoding='utf_8')
#el_espectador= pd.read_csv('el_espectador_2021_05_19_articles.csv',encoding='utf-8')
#el_tiempo= pd.read_csv('el_tiempo_2021_05_19_articles.csv',encoding='utf-8')
#el_pais= pd.read_csv('el_pais_2021_05_19_articles.csv',encoding='utf-8')

print(type(el_universal))
# print(el_universal.head) # imprimir primeros 5
# print(el_universal.tail) # imprimir últimos 5


# Index and selection

# dictionary like (estilo diccionario)
dir_like_title = el_universal['title']
dir_like_title_body = el_universal[['title', 'url']]

print(dir_like_title)
print(dir_like_title_body)


# Numpy like

numpy_like = el_universal.iloc[10:15]
print(numpy_like)

numpy_like2 = el_universal.iloc[1]['title']
print(numpy_like2)

numpy_like3 = el_universal.iloc[:5, 0] #Primera columna (body), filas del 1 al 5
print(numpy_like3)

numpy_like4 = el_universal.loc[:, 'body' : 'title'] #Todos los registros desde el body al title
print(numpy_like4)




# Data wrangling

#1. Añadir el newspaper_uid al Dataframe

el_universal['newspaper_uid'] = 'eluniversal'

print(el_universal)

# 2. Obtener el host

from urllib.parse import urlparse

el_universal['host'] = el_universal['url'].apply(lambda url: urlparse(url).netloc)
print(el_universal)

counts_host = el_universal['host'].value_counts()
print(counts_host)


#Preguntar los títulos que no tienen datos

# Missing Data

#3. Rellenar datos faltantes
print('3. Rellenar datos faltantes')
missing_titles_mask = el_universal['title'].isna()
print(missing_titles_mask)

print('procesando datos...')
'''
missing_titles =    (el_universal[missing_titles_mask]['url']
                        .str.extract(r'(?P<missing_titles>[^/]+)')
                        .applymap(lambda title: title.split('-'))
                        .applymap(lambda title_word_list: ' '.join(title_word_list))
                    )
'''
missing_titles =    (el_universal[missing_titles_mask]['url']
                        .str.extract(r'(?P<missing_titles>[^/]+)')
                        .astype(str).applymap(lambda title: title.replace('-',' '))
                    )
print(missing_titles)

# Aditional cleanup

#4. añadir uid a las filas
import hashlib

uids=(el_universal
        .apply(lambda row: hashlib.md5(bytes(row['url'].encode())), axis=1) # axis=1 filas,  axis=0 columnas
        .apply(lambda hash_objects:hash_objects.hexdigest())
    )
el_universal['uid'] = uids
el_universal.set_index('uid', inplace=True)

print('Aditional Cleanup {}'.format(el_universal))

#
import re
stripped_body = (el_universal
                    .apply(lambda row: row['body'], axis=1)
                    .apply(lambda body: re.sub(r'(\n|\r)+',r'', body)
                    )
                )
print(stripped_body)



# Data Enrichement con NTKD

#6. tokenizar el título y el body

import nltk
from nltk.corpus import stopwords

# nltk.download('punkt')
# nltk.download('stopwords')

stop_words = set(stopwords.words('spanish'))

def tokenize_column(df, column_name):
    return  (df
                .dropna()
                .apply(lambda row: nltk.word_tokenize(row[column_name]), axis=1)
                .apply(lambda tokens: list(filter(lambda token: token.isalpha(), tokens)))
                .apply(lambda tokens: list(map(lambda token: token.lower(), tokens)))
                .apply(lambda word_list: list(filter(lambda word: word not in stop_words, word_list)))
                .apply(lambda valid_word_list: len(valid_word_list))
            )

el_universal['n_tokens_title'] = tokenize_column(el_universal, 'title')
el_universal['n_tokens_title'] = tokenize_column(el_universal, 'body')

print(el_universal)


# Duplicate values

# Eliminar duplicados
duplicados = el_universal['title'].value_counts()
print('duplicados {}'.format(duplicados))

# el_universal[el_universal['title']] == ' Cristiano Ronaldo ya es campeÃ³n de todo en EspaÃ±a, Inglaterra e Italia '

el_universal.drop_duplicates(subset=['title'], keep='first', inplace=True)

# Tabla final
print(el_universal)


# Descriptive analysis
clean_universal = pd.read_csv('el_universal_2021_05_19_articles.csv', encoding='utf_8') 

# print(clean_universal.describe())
#%matplotlib inline
#import matplotlib_terminal
#import matplotlib.pyplot as plt
#clean_universal['n_tokens_title'].plot(style='k.')
#plt.plot([0,1],[0,1])

# Elaboración sw histogramas para dos o más variables o dos o más dataset
'''
all_newspaper = pd.concat([clean_universal])
grouped = all_newspaper.groupby('newspaper_uid')
grouped.hist()
'''
# Visualización de valores mínimos, medios y máximos de uno o más datasets
'''
grouped['n_tokens_body'].agg(['min','mean','max'])
'''
# visualización en gráfica de curvas de una o más variables o datasets
'''
grouped.plot()
'''
