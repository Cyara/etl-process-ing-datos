import requests

url1 = 'https://mighty-taiga-81587.herokuapp.com/docs/'
url2 = 'https://www.platzi.com'
url3 = 'https://www.eltiempo.com'

response = requests.get(url3)

'''
print(dir(response))

print(response.status_code)
print(response.url)
print(response.links)
print(response.headers)
print(response.text)
'''

#Extracción de información del html

import bs4

soup = bs4.BeautifulSoup(response.text, 'html.parser')

#print(soup.title.text)
#print(soup.select('meta[name=description]')[0]['content'])

news_links = soup.select('.page-link')

news = [new['href'] for new in news_links]

print(len(news))

list(map(lambda x: print(x), news))