import argparse
import datetime
import csv
import logging
from os import write
# Configuración salida por consola
logging.basicConfig(level=logging.INFO)
import re #modulo de expresiones regulares

from requests.exceptions import HTTPError
from urllib3.exceptions import MaxRetryError

import news_page_objects
from common import config


is_well_formed_url = re.compile(r'^https?://.+/.+$') # https://www.example.com/home
is_root_path = re.compile(r'^/.+$') # /some-text
logger = logging.getLogger(__name__)

def _news_scraper(news_site_uid):
    host = config()['news_sites'][news_site_uid]['url']

    logging.info('Beginning scraper for {}'.format(host))
    logging.info('Finding links in homepage...')

    article_links = find_article_links_in_home(news_site_uid, host)
    logging.info('{} article links found in homepage'.format(len(article_links)))

    articles =[]
    for link in article_links:
        article = _fetch_article(news_site_uid, host, link)

        if article:
            logger.info('Article fetched!')
            articles.append(article)
            # print(article.title)

    print('CANT ARTICLES: {}'.format(len(articles)))
    _save_articles(news_site_uid, articles)

    

def find_article_links_in_home(news_site_uid,host):
    homepage = news_page_objects.HomePage(news_site_uid,host)
    return homepage.article_links

def _fetch_article(news_site_uid, host, link):
    logging.info('Start fetching article at {}'.format(link))
    article = None

    try:
        article = news_page_objects.ArticlePage(news_site_uid, _build_link(host, link))
    except (HTTPError, MaxRetryError) as e:
        logger.warn('Error while fetching article!', exc_info=False)

    if article and not article.body:
        logger.warn('Invalid article. There is no body.')
        return None
    return article
        

def _build_link(host, link):
    if is_well_formed_url.match(link):
        return link
    elif is_root_path.match(link):
        return '{host}{uri}'.format(host=host, uri=link)
    else:
        return '{host}/{uri}'.format(host=host, uri=link)

def _save_articles(news_site_uid, articles):
    now = datetime.datetime.now().strftime('%Y_%m_%d')
    out_file_name = '{news_site_uid}_{datetime}_articles.csv'.format(
                                                                        news_site_uid=news_site_uid,
                                                                        datetime = now
                                                                    )
    csv_headers = list( 
                        filter(lambda property: not property.startswith('_'), dir(articles[0]))
                      )
    
    with open(out_file_name, mode='w+') as f:
        write = csv.writer(f)
        write.writerow(csv_headers)

        for article in articles:
            row = [str(getattr(article, prop)) for prop in csv_headers]
            write.writerow(row)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    news_sites_choices = list(config()['news_sites'].keys())

    parser.add_argument('news_sites',
                        help='the news site that you want to scrape',
                        type=str,
                        choices=news_sites_choices,
                       )
    
    args = parser.parse_args()
    _news_scraper(args.news_sites)